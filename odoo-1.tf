resource "digitalocean_droplet" "odoo-1" {
  image = "ubuntu-21-04-x64"
  name = "odoo-1"
  region = "nyc1"
  size = "s-1vcpu-1gb"
  private_networking = true
  ssh_keys = [
    data.digitalocean_ssh_key.terraform.id
  ]
  connection {
    host = self.ipv4_address
    user = "root"
    type = "ssh"
    agent = true
    timeout = "2m"
  }

  provisioner "file" {
    source      = "build.sh"
    destination = "/tmp/build.sh"
  }


  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/build.sh",
      "/tmp/build.sh"
    ]
  }
}
