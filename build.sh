#!/bin/bash
curl -fsSL https://get.docker.com | bash
wget https://github.com/docker/compose/releases/download/1.29.2/docker-compose-Linux-x86_64
mv docker-compose-Linux-x86_64 /usr/local/bin/docker-compose && chmod +x /usr/local/bin/docker-compose
### clonando, buildando e rodando aplicação
docker network create odoo-net
git clone -b odoo-v1 --depth=1 https://gitlab.com/pixi3s/devinf.git odoo-v1
cd odoo-v1/
docker-compose up -d --build
# rotina de restore odoo - preset

# reconfigure odoo.conf change db_name